package com.badlogic.gdx.graphics;

import com.badlogic.gdx.math.IntRectangle;
import com.badlogic.gdx.math.Rectangle;

public class PackerPixmap extends com.badlogic.gdx.graphics.Pixmap {
	private IntRectangle rect;
	private String name;
	
	public PackerPixmap(Pixmap pixmap, Rectangle rect, String name) {
		super(pixmap.pixmap);
		this.rect = new IntRectangle(rect);
		this.name = name;
	}
	
	public PackerPixmap(Pixmap pixmap, IntRectangle rect, String name) {
		super(pixmap.pixmap);
		this.rect = rect;
		this.name = name;
	}
	
	public int getWidth() {
		return rect.width;
	}
	
	public int getHeight() {
		return rect.height;
	}
	
	public String getName() {
		return name;
	}
	
}
