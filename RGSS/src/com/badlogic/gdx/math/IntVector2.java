package com.badlogic.gdx.math;

public class IntVector2 {
	public int x;
	public int y;
	
	public IntVector2(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void set(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(IntVector2 vector) {
		return x == vector.x && y == vector.y;
	}
}
