package com.badlogic.gdx.math;

public class IntRectangle {
	public int x;
	public int y;
	public int width;
	public int height;
	
	public IntRectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public IntRectangle(Rectangle rect) {
		this.x = Math.round(rect.x);
		this.y = Math.round(rect.y);
		this.width = Math.round(rect.width);
		this.height = Math.round(rect.height);
	}

	public void set(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public String toString () {
		return x + "," + y + "," + width + "," + height;
	}
}
