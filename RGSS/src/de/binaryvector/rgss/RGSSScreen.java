package de.binaryvector.rgss;

import java.util.Collections;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.TimeUtils;

import de.binaryvector.rgss.classes.DrawableContainer;
import de.binaryvector.rgss.classes.RGSSInput;

public class RGSSScreen extends Stage {
	
	public RGSSScreen(SpriteBatch batch) {
		super(640, 480, false, batch);
		((OrthographicCamera) this.getCamera()).setToOrtho(true);
		Assets.setup();
		Interpreter.setup();
        Interpreter.run();
        //Assets.setup();
        
        this.addListener(new InputListener() {
        	public boolean keyDown (InputEvent event, int keycode) {
        		RGSSInput.keys[keycode] = true;
        		return false;
        	}

        	public boolean keyUp (InputEvent event, int keycode) {
        		RGSSInput.keys[keycode] = false;
        		return false;
        	}
        });
        
        TextureRegionDrawable tex;
        TouchpadStyle style = new TouchpadStyle(
        		new TextureRegionDrawable(
        				new TextureRegion(new Texture("test.png"))),
        		new TextureRegionDrawable(
                		new TextureRegion(new Texture("test2.png")))
        		);
        Touchpad pad = new Touchpad(10, style);
        pad.setPosition(640 - 100, 480 - 100);
        pad.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Touchpad pad = (Touchpad) actor;
				for(int i = 19; i <= 22; i++) {
					RGSSInput.keys[i] = false;
				}
				if (Math.abs(pad.getKnobPercentX()) > Math.abs(pad.getKnobPercentY())) {
					if(pad.getKnobPercentX() > 0) {
						RGSSInput.keys[22] = true;
					} else {
						RGSSInput.keys[21] = true;
					}
				} else {
					if(pad.getKnobPercentY() > 0) {
						RGSSInput.keys[20] = true;
					} else if(pad.getKnobPercentY() < 0) {
						RGSSInput.keys[19] = true;
					}
				} 
			}
        	
        });
        this.addActor(pad);
        
        Image im = new Image(new TextureRegionDrawable(
				new TextureRegion(new Texture("test.png"))));
        im.addListener(new InputListener() {
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		RGSSInput.keys[66] = true;
        		return true;
        	}
        	
        	public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
        		Interpreter.dispose();
        		RGSSInput.keys[66] = false;
        	}
        });
        im.setPosition(pad.getX()-100, pad.getY());
        this.addActor(im);
        
	}
	
	public void draw() {
		
		if(Interpreter.canRender()) {
			
			Gdx.gl.glClearColor(1, 1, 1, 1);
			Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			Assets.update();
			
			while(!Interpreter.checks.isEmpty()) {
				Interpreter.checks.pop().check();
			}
			Collections.sort(Interpreter.sprites, Interpreter.comp);
			getSpriteBatch().begin();
			for(DrawableContainer sprite:Interpreter.sprites) {
				sprite.draw(getSpriteBatch(), sprite.z);
			}
			
			Interpreter.wakeUp();
			getSpriteBatch().end();
			
		}
		
		super.draw();
		
		Interpreter.update();
		//Interpreter.finishedRender();
	}
}
