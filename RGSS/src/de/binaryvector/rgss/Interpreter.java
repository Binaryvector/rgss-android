package de.binaryvector.rgss;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

import org.jruby.CompatVersion;
import org.jruby.RubyArray;
import org.jruby.RubyInstanceConfig.CompileMode;
import org.jruby.RubyThread;
import org.jruby.embed.ScriptingContainer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

import de.binaryvector.rgss.classes.DrawableContainer;
import de.binaryvector.rgss.classes.RGSSCheckable;

public class Interpreter {
	
	private static ScriptingContainer container;
	private static Thread script;
	public static RubyThread thread;
	private static int canRender;
	private static long time;
	public static LinkedList<DrawableContainer> sprites;
	public static LinkedList<RGSSCheckable> checks;
	public static Comparator<DrawableContainer> comp = new Comparator<DrawableContainer>() {

		@Override
		public int compare(DrawableContainer arg0, DrawableContainer arg1) {
			if(arg0.z < arg1.z) {
				return -1;
			} else if (arg0.z > arg1.z){
				return 1;
			}
			return 0;
		}
		
	};
	public static String scripts;
	public static String error;
	public static int count = 0;
	private static int check = 0;
	public static float lps = 0;
	
	private static String[] loadOrder = new String[] {
			"modules.rb",
			"classes.rb",
			"bitmap.rb",
			"sprite.rb",
			"window.rb",
			"rpg_cache.rb",
			"rpg.rb"
	};
	
	private static String[] startOrder = new String[] {
			"load_data.rb",
			"start.rb"	
	};
	
	public static void setup() {
		error = null;
		sprites = new LinkedList<DrawableContainer>();
		checks = new LinkedList<RGSSCheckable>();
	}
	
	public static void run() {
		canRender = 0;
		
		System.setProperty("jruby.bytecode.version", "1.6");
		//System.setProperty("jruby.compile.invokedynamic", "true");
		//System.out.println(Gdx.files.getExternalStoragePath());
		container = new ScriptingContainer();
		container.setCompatVersion(CompatVersion.RUBY1_8);
		container.setCurrentDirectory(Gdx.files.getExternalStoragePath()+"/Project 1");//"E:/LibGDX/RGSS/RGSS-android/assets/Project 1");
		//container.setCompileMode(CompileMode.FORCE);
		for(String file:loadOrder) {
			System.out.println(file);
				container.runScriptlet(Gdx.files.internal("ruby/"+file).readString());
		}
		container.runScriptlet(Gdx.files.internal("ruby/load_data.rb").readString());
		RubyArray scripts = (RubyArray) container.get("$RGSS_SCRIPTS");
		final Iterator iterator = scripts.iterator();
		script = new Thread(new Runnable() {

			@Override
			public void run() {
				while(iterator.hasNext()) {
					
					container.runScriptlet((String)iterator.next());
				}
			}
			
		});
		script.setName("RGSSInterpreter");
		script.start();
		
		/*
		script = (RubyThread) container.runScriptlet(Gdx.files.internal("ruby/start.rb").readString());
		//System.out.println(container.get("$RGSS_SCRIPTS"));
		script.run(); 
		//System.out.println(script);
		//System.out.println(thread);
		 */
		 
	}
	
	public static boolean allowRender() {//synchronized
		//System.out.println("game thread stop");
		//canRender += 1;
		
		synchronized(script){
		    //set ready flag to true (so isReady returns true)
			canRender += 1;
		    if(canRender > 2) {
		    	//lps = 1/(Math.max((TimeUtils.millis() - time), 1) / 1000.0f / canRender);
		    	script.notifyAll();
		    	Gdx.graphics.requestRendering();
			    try {
					script.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    //time = TimeUtils.millis();
		    }
		}
		
		
		//Gdx.app.postRunnable(new Runnable() {
        //	public void run() {
        		
        //	}
        //});
		return true;
		//sleep();
		
	}
	
	public static boolean canRender() {
		try {
			synchronized(script){
			    while (canRender == 0){
			        script.wait();
			    }
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//System.out.println(lps);
		canRender = 0;
		return true;
		/*
		if(canRender > 1) {//script.status().asJavaString().equals("sleep")) {
			canRender = 0;
			return true;
		}
		
		if(error != null) {
			String a = null;
			a += "1";
		}
		return false;
		*/
	}
	
	public static void wakeUp() {
		synchronized(script){
		    //set ready flag to true (so isReady returns true)
		    script.notifyAll();
		}
		
		/*
		time = TimeUtils.millis();
		script.run();
		*/
	}
	
	public static void update() {
		/*
		if(TimeUtils.millis() - time > 500) {
			if(script.status().asJavaString().equals("sleep")) {
				Gdx.app.log("RubyThread", "sleeping too long: "+(TimeUtils.millis() - time));
				wakeUp();
				
			}
		}
		*/
	}
	
	public static void dispose() {
		//script.
	}
}
