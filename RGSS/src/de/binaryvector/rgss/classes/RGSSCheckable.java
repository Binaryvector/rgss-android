package de.binaryvector.rgss.classes;

public interface RGSSCheckable {
	public void check();
}
