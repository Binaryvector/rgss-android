package de.binaryvector.rgss.classes;

public class RGSSTable {
	private int width;
	private int height;
	private int depth;
	private int[][][] data;
	
	public RGSSTable(int width, int height, int depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.data = new int[width][height][depth];
	}
	
	public int get(int x, int y, int z) {
		return data[x % width][y % height][z % depth];
	}
	
	public int set(int x, int y, int z, int v) {
		return data[x][y][z] = v;
	}
	
	public static RGSSTable _load(int[] data) {
		return new RGSSTable(0,0,0);
	}
}
