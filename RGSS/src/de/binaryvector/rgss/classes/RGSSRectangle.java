package de.binaryvector.rgss.classes;

public class RGSSRectangle {
	protected int x;
	public int y;
	public int width;
	public int height;
	
	public RGSSRectangle(int x, int y, int w, int h) {
		super();
		set(x, y, w, h);
	}
	
	//public RGSSRectangle(RGSSRectangle rect) {
	//	this(rect.x, rect.y, rect.width, rect.height);
	//}
	
	public void set(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
	}
	
	public void empty() {
		set(0, 0, 0, 0);
	}
	
	public int getx() {
		return x;
	}
	
	public int setx(int n) {
		return x = n;
	}
}
