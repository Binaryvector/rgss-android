package de.binaryvector.rgss.classes;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class DrawableContainer implements RGSSDrawable {
	private RGSSDrawable content;
	public int z;
	
	public DrawableContainer(RGSSDrawable content, int z) {
		this.content = content;
		this.z = z;
	}
	
	@Override
	public void draw(SpriteBatch batch, int z) {
		content.draw(batch, z);
	}

}
