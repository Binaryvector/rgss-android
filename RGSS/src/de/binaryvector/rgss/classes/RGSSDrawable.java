package de.binaryvector.rgss.classes;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface RGSSDrawable {
	public void draw(SpriteBatch batch, int z);
}
