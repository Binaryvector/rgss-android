package de.binaryvector.rgss.classes;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.IntRectangle;
import com.badlogic.gdx.math.Vector2;

import de.binaryvector.rgss.Interpreter;

public class RGSSSprite extends Sprite implements RGSSDrawable {

	private RGSSBitmap bitmap;
	public IntRectangle srcRect = new IntRectangle(0,0,0,0);
	private DrawableContainer container;
	private int z;
	private boolean visible = true;
	public Vector2 oPosition = new Vector2(0,0);
	public Vector2 position = new Vector2(0,0);
	
	public RGSSSprite() {
		super();
		container = new DrawableContainer(this, 0);
		Interpreter.sprites.add(container);
	}
	
	public RGSSSprite(Object viewport) {
		this();
	}
	
	public RGSSBitmap bitmap() {
		return bitmap;
	}
	
	public RGSSBitmap setBitmap(RGSSBitmap bitmap) {
		//System.out.println(bitmap.getName());
		//System.out.println(bitmap.getWidth());
		srcRect.set(0, 0, bitmap.getWidth(), bitmap.getHeight());
		return this.bitmap = bitmap;
	}

	public void draw(SpriteBatch batch, int z) {
		if(bitmap == null) {
			return;
		}
		
		if(!bitmap.isCreated()) {
			bitmap.create();
			this.setRegion(bitmap.getRegion());
			this.setSize(this.getRegionWidth(), this.getRegionHeight());
		}
		
		updateRegion();
		//System.out.println("draw "+z);
		super.draw(batch);
	}
	
	private void updateRegion() {
		this.setRegion(bitmap.getRegion());
		this.setRegion((this.getRegionX() + srcRect.x),
				(this.getRegionY() - bitmap.getHeight() + srcRect.height + srcRect.y),
				(srcRect.width), (-srcRect.height));
		//this.setRegion(this.getRegionX(),
		//				this.getRegionY(),
		//				this.getRegionWidth(), this.getRegionHeight());
		//System.out.println(this.getRegionX());

		this.setBounds(position.x - oPosition.x, position.y - oPosition.y,
				this.getRegionWidth(), this.getRegionHeight());
		
	}
	
	public void dispose() {
		Interpreter.sprites.remove(container);
		container = null;
	}
	
	public void setZ(int z) {
		this.z = z;
		container.z = z;
	}
	
	public int getZ() {
		return z;
	}
	
	public boolean getVisible() {
		return visible;
	}
	
	public boolean setVisible(boolean n) {
		if(visible != n) {
			if(n) {
				Interpreter.sprites.add(container);
			} else {
				Interpreter.sprites.remove(container);
			}
		}
		return visible = n;
	}
}
