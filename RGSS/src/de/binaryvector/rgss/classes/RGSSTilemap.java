package de.binaryvector.rgss.classes;

import java.util.LinkedList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.IntVector2;

import de.binaryvector.rgss.Interpreter;

public class RGSSTilemap implements RGSSCheckable, RGSSDrawable {
	public RGSSTable map_data;
	public RGSSTable priorities;
	public IntVector2 oPosition = new IntVector2(0,0);
	private IntVector2 oldPosition = new IntVector2(0,0);;
	private RGSSBitmap tileset;
	//private TextureRegion[][] tiles;
	private DrawableContainer[] containers = new DrawableContainer[15 + 5];//16*5+1? 15*5+1?
	//private FrameBuffer[] buffers = new FrameBuffer[16 * 5 + 1];
	
	private float[][] vertices;
	
	private LinkedList<int[]> layers = new LinkedList<int[]>();
	//private float[][] vertices = new float[16*5+1][4*5 * 16*21*3];
	
	private final int X1 = 0;
	private final int Y1 = 1;
	private final int C1 = 2;
	private final int U1 = 3;
	private final int V1 = 4;
	
	
	public RGSSTilemap() {
		for(int i = 0; i < containers.length; i++) {
			containers[i] = new DrawableContainer(this, i * 32 + 32);
			Interpreter.sprites.add(containers[i]);
		}
		containers[0].z = 0;
		Interpreter.checks.add(this);
	}
	
	public void setTileset(RGSSBitmap bitmap) {
		tileset = bitmap;
	}
	
	public void check() {
		if(!tileset.isCreated()) {
			tileset.create();
		}
		
		if(tileset.getRegion().isFlipY()) {
			tileset.getRegion().flip(false, true);
		}
		TextureRegion[][] tiles = tileset.getRegion().split(32, 32);
		tileset.getRegion().flip(false, true);
		
		float color = Color.WHITE.toFloatBits();
		vertices = new float[(tileset.getHeight() / 32) * 8][];
		for(int i = 0; i < vertices.length; i++) {
			TextureRegion region = tiles[i / 8][i % 8];
			vertices[i] = new float[] {
				0, 0, color, region.getU2(), region.getV(),
				0, 0, color, region.getU(), region.getV(),
				0, 0, color, region.getU(), region.getV2(),
				0, 0, color, region.getU2(), region.getV2()
			};
		}
		
	}
	
	
	
	public void update() {
		if(!oldPosition.equals(oPosition)) {
			
			oldPosition.set(oPosition.x, oPosition.y);
		}
	}

	@Override
	public void draw(SpriteBatch batch, int z1) {
		int layer = z1 / 32;
		if (layer > 0) {
			layer -= 1;
		}
		//if(layer > -1) {
		//	return;
		//}
		int posX = (oPosition.x) / 32;
		int posY = (oPosition.y) / 32;
		
		int id;
		int rx;
		int ry;
		
		int endY = Math.min(16, layer);
		int startY = 0;
		if(layer == 0) {
			endY = 16;
		}
		if(layer > 5) {
			startY = layer - 5;
		}
		
		for(int x = 0; x < 21; x++) {
			rx = x * 32 - oPosition.x % 32;
			for(int y = startY; y < endY; y++) {
				ry = y * 32 - oPosition.y % 32;
				for(int z = 0; z < 3; z++) {
					id = map_data.get(x + posX, y + posY, z);
					if(layer != 0) {
						if(y + priorities.get(id, 0, 0) != layer) {
							continue;
						}
					} else {
						if (priorities.get(id, 0, 0) != 0) {
							continue;
						}
					}
					//System.out.println(id);
					if(id >= 384) {
						id -= 384;
						//batch.draw(tiles[id / 8][id % 8], rx, ry);
						
						vertices[id][5] = vertices[id][2*5] = rx;
						vertices[id][1] = vertices[id][1*5+1] = ry;
						vertices[id][0] = vertices[id][3*5] = rx + 32;
						vertices[id][2*5+1] = vertices[id][3*5+1] = ry + 32;
						batch.draw(tileset.getRegion().getTexture(), vertices[id], 0, 5 * 4);
						
					}
				}
			}
		}
		
	}
}
