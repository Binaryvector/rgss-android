package de.binaryvector.rgss.classes;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.PackerPixmap;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.binaryvector.rgss.Assets;

public class RGSSBitmap {
	
	private TextureRegion region;
	private PackerPixmap pixmap;
	private String path;
	private int width;
	private int height;
	
	public RGSSBitmap(String path) {
		System.out.println("load "+path);
		this.path = path;
		pixmap = Assets.loadPixmap(path);
		width = pixmap.getWidth();
		height = pixmap.getHeight();
	}
	
	public RGSSBitmap(int width, int height) {
		this.width = width;
		this.height = height;
		pixmap = Assets.loadPixmap(width, height);
	}
	
	public boolean isCreated() {
		return region != null;
	}
	
	public TextureRegion getRegion() {
		return region;
	}
	
	public void create() {
		region = Assets.getImage(pixmap.getName());
	}
	
	public String getName() {
		return pixmap.getName();
	}
	
	public int getWidth() {
		return width;//region.getRegionWidth();
	}
	
	public int getHeight() {
		return height;//region.getRegionHeight();
	}

	public TextureRegion getSkinRegion() {
		return new TextureRegion(region, 128, -128, 64, -64);
	}

	public TextureRegion getBackgroundRegion() {
		TextureRegion result = new TextureRegion(region);
		result.setRegionWidth(128);
		return result;
	}
	
	public TextureRegion getCursorRegion() {
		return new TextureRegion(region,128,-32,32,-32);
	}
}
