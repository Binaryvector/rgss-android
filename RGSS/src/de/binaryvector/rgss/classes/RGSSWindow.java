package de.binaryvector.rgss.classes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatchNoCenter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.IntRectangle;
import com.badlogic.gdx.math.Rectangle;

import de.binaryvector.rgss.Interpreter;

public class RGSSWindow extends IntRectangle implements RGSSDrawable {
	private NinePatchNoCenter frame;
	private RGSSWindowskin windowskin;
	private RGSSBitmap content;
	private int counter;
	public IntRectangle cursor;
	public boolean visible = true;
	public boolean active = true;
	private DrawableContainer container;
	private int z;
	
	
	public RGSSWindow(int x, int y, int w, int h) {
		super(x, y, w, h);
		container = new DrawableContainer(this, 0);
		Interpreter.sprites.add(container);
		counter = 0;
	}
	
	@Override
	public void draw(SpriteBatch batch, int z) {
		if(!visible) {
			return;
		}
		if(frame == null) {
			//System.out.println(1);
			if(windowskin == null) {
				//System.out.println(2);
				return;
			} else if(!windowskin.isCreated()) {
				//System.out.println(3);
				windowskin.create();
			}
			//System.out.println(4);
			frame = new NinePatchNoCenter(windowskin.getSkinRegion(),8,8,8,8);
		}
		//System.out.println("jo");
		//System.out.println("window "+z);
		batch.draw(windowskin.getBackgroundRegion(), x, y, width, height);
		frame.draw(batch, x, y, width, height);
		
		batch.setColor(1, 1, 1, 1 - counter / 120.0f);
		batch.draw(windowskin.getCursorRegion(), x + cursor.x, y + cursor.y,
				cursor.width, cursor.height);
		batch.setColor(Color.WHITE);
	}
	
	public void setWindowskin(RGSSBitmap bitmap) {
		windowskin = new RGSSWindowskin(bitmap);
	}
	
	public void update() {
		counter += 1;
		counter = counter % 60;
	}
	
	public void dispose() {
		Interpreter.sprites.remove(container);
		container = null;
	}
	
	public void setZ(int z) {
		this.z = z;
		container.z = z;
	}
	
	public int getZ() {
		return z;
	}
}
