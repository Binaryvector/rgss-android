package de.binaryvector.rgss.classes;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class RGSSWindowskin {
	private TextureRegion background;
	private TextureRegion frame;
	private TextureRegion cursor;
	private RGSSBitmap bitmap;
	
	public RGSSWindowskin(RGSSBitmap bitmap) {
		this.bitmap = bitmap;
	}
	
	public boolean isCreated() {
		return bitmap.isCreated() && background != null && frame != null && cursor != null;
	}
	
	public void create() {
		if(!bitmap.isCreated()) {
			bitmap.create();
		}
		background = bitmap.getBackgroundRegion();
		frame = bitmap.getSkinRegion();
		cursor = bitmap.getCursorRegion();
	}
	
	public TextureRegion getSkinRegion() {
		return frame;
	}
	
	public TextureRegion getBackgroundRegion() {
		return background;
	}
	
	public TextureRegion getCursorRegion() {
		return cursor;
	}
}
