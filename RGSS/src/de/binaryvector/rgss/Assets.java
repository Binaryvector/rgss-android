package de.binaryvector.rgss;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.PackerPixmap;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.TextureAtlasData.Region;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class Assets {
	
	private static PixmapPacker packer;
	private static TextureAtlas atlas;
	private static int id;
	private static String project = "Project 1";
	
	public static void setup() {
		packer = new PixmapPacker(1024, 1024 * 2, Pixmap.Format.RGBA8888, 0, false);
		atlas = packer.generateTextureAtlas(TextureFilter.Linear, TextureFilter.Linear, false);
	}
	
	public static PackerPixmap loadPixmap(String path) {
		FileHandle file = Gdx.files.external(project+"/"+path);
		for(String ext:new String[] {".jpg",".png"}) {
			file = Gdx.files.external(project+"/"+path+ext);
			if(file.exists()) {
				System.out.println("found" + file.name());
				break;
			}
		}
		String name = file.pathWithoutExtension();
		Rectangle rect = packer.getRect(name);
		if(rect == null) {
			System.out.println("create pixmap");
			Pixmap pix = new Pixmap(file);
			packer.pack(name, pix);
			rect = packer.getRect(name);
			pix.dispose();
		}
		return new PackerPixmap(packer.getPage(name).getPixmap(), rect, name);
	}
	
	public static PackerPixmap loadPixmap(int width, int height) {
		Pixmap pix = new Pixmap(width, height, Format.RGBA8888);
		String name = "" + width + "." + height + "." + id;
		id += 1;
		packer.pack(name, pix);
		pix.dispose();
		return new PackerPixmap(packer.getPage(name).getPixmap(), packer.getRect(name), name);
	}
	
	public static void update() {
		packer.updateTextureAtlas(atlas, TextureFilter.Linear, TextureFilter.Linear, false);
	}

	public static TextureRegion getImage(String name) {
		Gdx.app.log("Asstes", "get image "+name);
		TextureRegion result = atlas.findRegion(name);
		if(result == null) {
			Gdx.app.log("Asstes", "couldn't find image "+name);
			for(AtlasRegion region:atlas.getRegions()) {
				System.out.println(region.name);
			}
			return new TextureRegion(new Texture(1,1,Format.RGBA8888));
		}
		result.flip(false, true);
		return result;
	}
	
	public static void dispose() {
		atlas.dispose();
	}
}
