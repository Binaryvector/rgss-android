package de.binaryvector.rgss;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.binaryvector.rgss.classes.RGSSDrawable;

public class RGSSMain implements ApplicationListener {
	private SpriteBatch batch;
	private Stage stage;
	@Override
	public void create() {		
		
		//Gdx.graphics.setContinuousRendering(false);
		batch = new SpriteBatch();
		stage = new RGSSScreen(batch);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void dispose() {
		Assets.dispose();
		Interpreter.dispose();
	}

	@Override
	public void render() {		
		
		stage.act();
		stage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
