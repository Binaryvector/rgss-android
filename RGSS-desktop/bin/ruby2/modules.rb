require "java"
module Graphics
	@frame_rate = 40
	class <<Graphics
		attr_accessor	:frame_count
		attr_accessor	:frame_rate
		def update
			@frame_count += 1
			Java::de.binaryvector.rgss.Interpreter.allowRender
			
			#Java::de.binaryvector.rgss.Interpreter.sleep
		end
		
		def frame_reset
		end
		
		def freeze
			
		end
		
		def transition(*args)
			
		end
		
		def setup
			@frame_count = 0
		end
	end
end

Graphics.setup

module Input
	KEYS = [
	DOWN = 20,
	UP = 19,
	LEFT = 21,
	RIGHT = 22,
	L = 0,
	R = 0,
	A = 0,
	B = 131,
	C = 66,
	D = 0,
	F5 = 0
	]
	class <<Input
		def update
			if(@keys.nil?)
				@keys = {}
			end
			for key in KEYS do
				if Java::de.binaryvector.rgss.classes.RGSSInput.keys[key]
					@keys[key] += 1
				else
					@keys[key] = 0
				end
			end
		end
		
		def trigger?(arg)
			return @keys[arg] == 1
		end
		
		def press?(arg)
			return @keys[arg] > 0
		end
		
		def repeat?(arg)
			return ((@keys[arg] >=16 and @keys[arg] % 8 == 0) or Input.trigger?(arg))
		end
		
		def dir4
			dirs = [DOWN , LEFT, RIGHT, UP]
			for i in 0...4 do
				if press?(dirs[i])
					return (i + 1) * 2
				end
			end
			return 0
		end
	end
end

module Audio
	class <<Audio
		def bgm_play(*args)
		end
		def bgm_stop
		end
		def bgm_fade(args)
		end
		def bgs_play(*args)
		end
		def bgs_stop
		end
		def bgs_fade(args)
		end
		def me_play(*args)
		end
		def me_stop
		end
		def me_fade(args)
		end
		def se_play(*args)
		end
		def se_stop
		end
	end
end