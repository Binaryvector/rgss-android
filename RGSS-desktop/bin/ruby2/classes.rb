require "java"
class Table
	attr_reader	:table
  def initialize(x, y = 1, z = 1)
    @table = Java::de.binaryvector.rgss.classes.RGSSTable.new(x, y, z)
    @xsize = x
    @ysize = y
    @zsize = z
  end
  
  def [](x, y = 0, z = 0)
    return @table.get(x,y,z)
  end
  
  def []=(*args)
    x = args[0]
    y = args.size > 2 ? args[1] : 0
    z = args.size > 3 ? args[2] : 0
    v = args.pop
    @table.set(x,y,z,v)
  end
  def _dump(d = 0)
	  dump = @table.dump
	  dump[0..4].pack('LLLLL') <<
	  dump[4..dump.size-1].pack("S#{xsize * ysize * zsize}")
	  return dump
    #[@dim, @xsize, @ysize, @zsize, @xsize * @ysize * @zsize].pack('LLLLL') <<
      #@data.pack("S#{@xsize * @ysize * @zsize}")
  end
  def self._load(s)
    size, nx, ny, nz, items = *s[0, 20].unpack('LLLLL')
    case size
    when 1; t = Table.new(nx); ny = nz = 1
    when 2; t = Table.new(nx, ny); nz = 1
    when 3; t = Table.new(nx, ny, nz)
    end
    data = s[20, items * 2].unpack("S#{items}")
    for i in 0...data.size do
	    t[i % nx, (i / nx) % ny, i / (nx * ny)] = data[i]
	end
    return t
  end
  attr_accessor(:xsize, :ysize, :zsize, :data)
end

class Color
	def initialize(r,g,b,a)
		
	end
	def self._load(stuff)
		
	end
	def set(*args)
	end
	
end

class Tone
	attr_accessor :red
	attr_accessor :green
	attr_accessor :blue
	attr_accessor :gray
	def initialize(r,g,b,gr)
		@red = r
		@green = g
		@blue = b
		@gray = gr
	end
	def self._load(stuff)
		return Tone.new(0,0,0,0)
	end
end

class Rect < Java::de.binaryvector.rgss.classes.RGSSRectangle
end

