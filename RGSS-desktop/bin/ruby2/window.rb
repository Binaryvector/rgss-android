require "java"
class Window
	attr_accessor	:pause
	def initialize(x=0, y=0, w=32, h=32)
		@_window = Java::de.binaryvector.rgss.classes.RGSSWindow.new(x, y, w, h)
		@_rect = Rect.new(0,0,0,0)
		@_window.cursor = @_rect
	end
	def update
		@_window.update
	end
	def active
		@_window.active
	end
	
	def active=(n)
		@_window.active = n
	end
	
	def visible
		@_window.visible
	end
	
	def visible=(n)
		@_window.visible = n
	end
	
	def ox
		return 0
	end
	
	def ox=(n)
		return 0
	end
	
	def oy
		return 0
	end
	
	def oy=(n)
		return 0
	end
	
	def cursor_rect
		return @_rect
	end
	
	def contents_opacity
		return 255
	end
	
	def contents_opacity=(n)
		return n
	end
	
	def back_opacity
		return 0
	end
	
	def back_opacity=(n)
		return 0
	end
	
	def opacity
		return 0
	end
	
	def opacity=(n)
		return 0
	end
	
	def x
		return @_window.x
	end
	
	def x=(n)
		return @_window.x = n
	end
	
	def y
		return @_window.y
	end
	
	def y=(n)
		return @_window.y = n
	end
	
	def z
		@_window.getZ
	end
	
	def z=(n)
		@_window.setZ(n)
	end
	
	def width
		return @_window.width
	end
	
	def width=(n)
		return @_window.width = n
	end
	
	def height
		return @_window.height
	end
	
	def height=(n)
		return @_window.height = n
	end
	
	def contents
		return @_contents
	end
	
	def contents=(bit)
		return @_contents = bit
	end
	
	def windowskin=(bit)
		@_window.setWindowskin(bit._bitmap)
		return @_windowskin = bit
	end
	
	def dispose
		@_window.dispose
	end
end