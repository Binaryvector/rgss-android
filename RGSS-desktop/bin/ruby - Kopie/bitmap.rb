require "java"
class Bitmap
	attr_reader	:_bitmap
	attr_accessor	:font
	def initialize(arg1, arg2=nil)
		@font = Font.new
		if arg2.nil? and arg1.is_a?(String)
			@_bitmap = Java::de.binaryvector.rgss.classes.RGSSBitmap.new(arg1)
		elsif arg1.is_a?(Numeric) and arg1.is_a?(Numeric)
			@_bitmap = Java::de.binaryvector.rgss.classes.RGSSBitmap.new(arg1, arg2)
		else
			@_bitmap = Java::de.binaryvector.rgss.classes.RGSSBitmap.new(0, 0)
		end
	end
	
	def clear
		
	end
	
	def width
		return @_bitmap.getWidth
	end
	
	def height
		return @_bitmap.getHeight
	end
	
	def blt(*args)
	end
	
	def hue_change(*args)
	end
	
	
	def fill_rect(*args)
		
	end
	
	def draw_text(*args)
		
	end
	
	def text_size(*args)
		return Rect.new(0,0,32,32)
	end
	
	def dispose
		
	end
	
	def rect
		return Rect.new(0,0,32,32)
	end
	
	def disposed?
		return false
	end
end

class Font
	attr_accessor	:color
	attr_accessor	:name
	attr_accessor	:size
	def initialize
		@name = ""
		@color = Color.new(255,255,255,255)
	end
	
	def self.default_name
		return "Arial"
	end
	
	def self.default_name=(n)
	end
	
	def self.default_size
		return 22
	end
	
	def self.default_size=(n)
	end
	
end