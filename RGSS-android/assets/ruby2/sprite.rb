class Sprite
	attr_reader	:bitmap
	attr_accessor	:opacity
	attr_accessor	:blend_type
	attr_accessor	:bush_depth
	attr_accessor	:zoom_x
	attr_accessor	:zoom_y
	attr_accessor	:angle
	attr_accessor	:tone
	attr_reader		:src_rect
	
	def initialize(viewport = nil)
		@sprite = Java::de.binaryvector.rgss.classes.RGSSSprite.new
		@src_rect = @sprite.srcRect.dup
		@z = 0
		@tone = Tone.new(0,0,0,0)
	end
	
	def x=(n)
		@sprite.position.x = n
	end
	
	def x
		return @sprite.position.x
	end
	
	def y=(n)
		@sprite.position.y = n
	end
	
	def y
		return @sprite.position.y
	end
	
	def ox
		return @sprite.oPosition.x
	end
	
	def ox=(n)
		return @sprite.oPosition.x = n
	end
	
	def oy
		return @sprite.oPosition.y
	end
	
	def oy=(n)
		return @sprite.oPosition.y = n
	end
	
	def z
		return @sprite.getZ
	end
	
	def visible
		return @sprite.getVisible
	end
	
	def visible=(n)
		@sprite.setVisible(n)
	end
	
	def z=(n)
		@sprite.setZ(n)
	end
	
	def mirror
		return false
	end
	
	def mirror=(n)
	end
	
	
	def update
	end
	
	def bitmap=(bit)
		@sprite.setBitmap(bit._bitmap)
		return @bitmap = bit
	end
	
	def dispose
		@sprite.dispose
	end
end

class Viewport
	attr_accessor	:rect
	attr_accessor	:z
	attr_accessor	:ox
	attr_accessor	:oy
	attr_accessor	:tone
	attr_accessor	:color
	def initialize(x, y, w, h)
		@rect = Rect.new(x, y, w, h)
		@z = @ox = @oy = 0
	end
	
	def update
	end
	
	def dispose
	end
	
end

class Tilemap
	attr_reader		:tileset
	attr_accessor	:autotiles
	attr_reader		:map_data
	attr_accessor	:flash_data
	attr_reader		:priorities
	attr_accessor	:visible
	def initialize(viewport)
		@_tilemap = Java::de.binaryvector.rgss.classes.RGSSTilemap.new
		@autotiles = []
	end
	
	def update
	end
	
	def dispose
	end
	
	def disposed?
	end
	
	def viewport
	end
	
	def tileset=(bitmap)
		@_tilemap.setTileset(bitmap._bitmap)
		@tileset = bitmap
	end
	
	def priorities=(table)
		@_tilemap.priorities = table.table
	end
	
	def map_data=(table)
		@_tilemap.map_data = table.table
	end
	
	def ox
		@_tilemap.oPosition.x
	end
	
	def ox=(n)
		@_tilemap.oPosition.x = n
	end
	
	def oy
		@_tilemap.oPosition.y
	end
	
	def oy=(n)
		@_tilemap.oPosition.y = n
	end
end
=begin
class Plane
	attr_accessor	:ox
	attr_accessor	:oy
	attr_accessor	:z
	attr_accessor	:zoom_x
	attr_accessor	:zoom_y
	attr_accessor	:bitmap
	attr_accessor	:visible
	attr_accessor	:opacity
	attr_accessor	:blend_type
	attr_accessor	:color
	attr_accessor	:tone
	def initialize(viewport = nil)
		
	end
end
=end
Plane = Sprite